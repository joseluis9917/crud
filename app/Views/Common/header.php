<nav class="main-header navbar navbar-static-top navbar-expand" >

  <ul class="navbar-nav">
    <li class="nav-item">
      <button class="nav-link btn" data-widget="pushmenu"><i class="fa-solid fa-bars"></i></button>
    </li>
  </ul>

    <ul class="navbar-nav ml-auto">
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" data-toggle="dropdown" id="navbarDropdown" aria-haspopup="true" role="button" aria-expanded="false">
          <span class="hidden-xs">Jose Luis Perez - FCC</span>
        </a>
        <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
          <a href="#" class="dropdown-item">Salir</a>
        </div>
      </li>
    </ul>
</nav>

<body class="skin-blue sidebar-mini layout-fixed sidebar-open" style="width: 100%; position: absolute; height:auto !important; min-height: 100%;">
  <div class="wrapper">