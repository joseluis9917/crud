<aside class="main-sidebar elevation-4">
<a class="brand-link logo-switch" href="<?=base_url('listView/'.'3');?>" style="background-color: #002D4A;">
  <img class="brand-image-xl logo-xl" src="<?=base_url('public/img/logo.png')?>" alt="BUAP Logo Large">
  <img class="brand-image-xs logo-xs img-circle elevation-2" src="<?=base_url('public/img/Escudo_0.png'); ?>" alt="BUAP Logo Small">
</a>
  <section class="sidebar os-host os-theme-light os-host-overflow os-host-overflow-y os-host-resize-disabled os-host-scrollbar-horizontal-hidden os-host-transition">
    <div class="user-panel mt-3 pb-3 mb-3 d-flex logo-switch">
      <div class="image">
        <img src="<?=base_url('public/img/Escudo_0.png'); ?>" class="img-circle elevation-2 logo-xl" alt="User Image">
        <img src="<?=base_url('public/img/Empty.png'); ?>" class="img-circle" alt="User Image">
      </div>
      <div class="info">
        <span>BUAP</span>
        </br>
        <span>Administrador</span>
      </div>
    </div>
    <nav class="mt-2">
      <ul class="nav nav-pills nav-sidebar  nav-child-indent flex-column" data-widget="treeview" role="menu" data-accordion="false">
        <li class="nav-header">MENÚ</li>
        <li class="nav-item">
          <a href="<?=base_url('listView/'.'3'); ?>" class="nav-link active">
            <i class="nav-icon fas fa-users"></i> 
            <p>Usuarios</p>
          </a>
        </li>
        <li class="nav-item">
          <a href="<?=base_url('daua/'.'3');?>" class="nav-link">
            <i class="nav-icon fas fa-building"></i>
            <p>DAUA</p>
          </a>
        </li>
        <li class="nav-item">
          <a href="<?=base_url('fetchLoadUsersView');?>" class="nav-link">
            <i class="nav-icon fas fa-building"></i>
            <p>Usuarios cargados</p>
          </a>
        </li>
        <li class="nav-item">
          <a href="<?=base_url('fetch-attendance'); ?>" class="nav-link">
            <i class="nav-icon fas fa-clipboard-user"></i>
            <p>Asistencia</p>
          </a>
        </li>
        <li class="nav-item">
          <a href="https://boletin.buap.mx" target="_blank" class="nav-link">
            <i class="nav-icon far fa-file"></i> 
            <p>Boletines</p>
          </a>
        </li>
      </ul>
    </nav>
  </section>
</aside>

<link rel="stylesheet" href="https://cdn.datatables.net/select/1.3.3/css/select.dataTables.min.css" />
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs/jszip-2.5.0/dt-1.10.18/b-1.5.6/b-colvis-1.5.6/b-flash-1.5.6/b-html5-1.5.6/b-print-1.5.6/r-2.2.2/datatables.min.css" />
<div class="skin-blue">
    <div class="content-wrapper" >