</div>
<footer class="main-footer footer-buap">
    <div class="col-md-12" align="center" style="position:relative; padding:16px; font-size:12px;"  >
      <div class="col-md-4 col-xs-12" style="color:white; padding-top:10px;">
        <img class="img-fluid"  src="<?php echo base_url('public/img/logo.png');?>" />
      </div>
      <div class="col-md-4 col-xs-12" style="color:white; padding-top:10px;"></div>
      <div class="col-md-4 col-xs-12" style="color:white; padding-top:10px;">
        <a class="url-hf" href="http://www.buap.mx/privacidad" target="_blank">Aviso de Privacidad</a><br>
      </div>
    </div>
</footer>
</div>



<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.13.1/js/jquery.dataTables.js"></script>
<script lang="javascript" src="https://cdn.sheetjs.com/xlsx-0.19.2/package/dist/xlsx.full.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/js/bootstrap.bundle.min.js" integrity="sha384-w76AqPfDkMBDXo30jS1Sgez6pr3x5MlQ1ZAGC+nuZB+EYdgRZgiwxhTBTkF7CXvN" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/admin-lte@3.2/dist/js/adminlte.min.js"></script>

</div>
</body>


</html>
<script type="application/javascript">
    $(document).ready( function () {
        $(function () {
            var url = window.location;
            
            $('ul.nav-sidebar a').filter(function() {
                $("ul.nav-sidebar a").removeClass("active");
                return this.href == url;
            }).addClass('active');

            $('ul.nav-treeview a').filter(function() {
                $("ul.nav-treeview a").removeClass("active");
                return this.href == url;
            }).parentsUntil(".nav-sidebar > .nav-treeview").addClass('menu-open').prev('a').addClass('active');
        }); 
    } );
</script>