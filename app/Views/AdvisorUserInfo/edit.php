<?=$common?>
<?=$header?>
<?=$menu?>
<section class="content-header">
    <div class="container-fluid">
        <h1>
            <i class="fas fa-users"></i>
            Usuarios
        </h1>
    </div>
</section>
<section class="content">
    <div class="container-fluid">
        <div class="card">
            <div class="card-body">
                <h5 class="card-title">Ingresar datos de usuario:</h5>
                <p class="card-text">
                    <?php foreach ($errors as $error): ?>
                        <li><?= esc($error) ?></li>
                    <?php endforeach ?>
                    <?php  
                        if (isset($user)){
                                echo form_open('process/'.$user['id'], 'class="needs-validation" novalidate');
                            } 
                            else {
                                echo form_open('process','class="needs-validation" novalidate');
                            }
                    ?>
                        <div class="form-group">
                            <label class="form-label" for="id">ID:</label>
                            <input <?php if (isset($user)){echo "value=".$user['id'];}else{echo "value='' ";} ?> id="id" class="form-control" name="id" <?php if (isset($user)){echo "readonly=readonly";} ?> required="" placeholder="">
                            <div class="invalid-feedback">
                                ID valido es requerido.
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="form-label" for="grado">Grado:</label>
                            <input value="<?php if (isset($user)){echo $user['grado'];} else { echo'';}?>" id="grado" class="form-control" type="text" name="grado" required=''>
                            <div class="invalid-feedback">
                                Grado valido es requerido.
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="form-label" for="nombre">Nombre:</label>
                            <input value="<?php if (isset($user)){ echo $user['nombre'];} else { echo'';}?>" id="nombre" class="form-control" type="text" name="nombre" required=''>
                            <div class="invalid-feedback">
                                Nombre valido es requerido.
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="form-label" for="apellido_paterno">Apellido Paterno:</label>
                            <input value="<?php if (isset($user)){ echo $user['apellido_paterno'];} else { echo'';}?>" id="apellido_paterno" class="form-control" type="text" name="apellido_paterno" required=''>
                            <div class="invalid-feedback">
                                Apellido paterno valido es requerido.
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="form-label" for="apellido_materno">Apellido Materno:</label>
                            <input value="<?php if (isset($user)){ echo $user['apellido_paterno'];} else { echo'';}?>" id="apellido_materno" class="form-control" type="text" name="apellido_materno" required=''>
                            <div class="invalid-feedback">
                                Apellido materno valido es requerido.
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="form-label" for="cumpleanos">Cumpleaños:</label>
                            <input value="<?php if (isset($user)){ echo $user['cumpleanos'];} else {echo'';}?>" id="cumpleanos" class="form-control" type="date" name="cumpleanos" required=''>
                            <div class="invalid-feedback">
                                Ingrese una fecha valida.
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="form-label" for="cargo">Cargo:</label>
                            <input value="<?php if (isset($user)){ echo $user['cargo'];} else {echo'';}?>" id="cargo" class="form-control" type="number" name="cargo" required=''>
                            <div class="invalid-feedback">
                                Cargo valido requerido.
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="form-label" for="tipo">Tipo:</label>
                            <input value="<?php if (isset($user)){ echo $user['tipo'];} else {echo'';}?>" id="tipo" class="form-control" type="number" name="tipo" required=''>
                            <div class="invalid-feedback">
                                Tipo valido requerido.
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="form-label" for="daua">DAUA:</label>
                            <input value="<?php if (isset($user)){ echo $user['daua'];} else {echo'';}?>" id="daua" class="form-control" type="text" name="daua" required=''>
                            <div class="invalid-feedback">
                                Ingrese una DAUA valida.
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="form-label" for="tel_cel_1">Telefono Celular 1:</label>
                            <input value="<?php if (isset($user)){ echo $user['tel_cel_1'];} else {echo'';}?>" id="tel_cel_1" class="form-control" type="tel"  name="tel_cel_1" required=''>
                            <div class="invalid-feedback">
                                Ingrese un número telefónico valido.
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="form-label" for="tel_cel_1_whats">Telefono Celular Whatsapp 1:</label>
                            <input value="<?php if (isset($user)){ echo $user['tel_cel_1_whats'];} else {echo'';}?>" id="tel_cel_1_whats" class="form-control" type="number" name="tel_cel_1_whats" maxlength="1">
                            <div class="invalid-feedback">
                                Ingrese un número telefónico valido.
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="form-label" for="tel_cel_2">Telefono Celular 2:</label>
                            <input value="<?php if (isset($user)){ echo $user['tel_cel_2'];} else {echo'';}?>" id="tel_cel_2" class="form-control" type="text" name="tel_cel_2">
                            <div class="invalid-feedback">
                                Ingrese un número telefónico valido.
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="form-label" for="tel_cel_2_whats">Telefono Celular 2 Whatsapp:</label>
                            <input value="<?php if (isset($user)){ echo $user['tel_cel_2_whats'];} else { echo'';}?>" id="tel_cel_2_whats" class="form-control" type="number" name="tel_cel_2_whats" maxlength="1" >
                            <div class="invalid-feedback">
                                Ingrese un número telefónico valido.
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="form-label" for="tel_casa">Telefono Casa:</label>
                            <input value="<?php if (isset($user)){ echo $user['tel_casa'];} else {echo'';}?>" id="tel_casa" class="form-control" type="text" name="tel_casa" required=''>
                            <div class="invalid-feedback">
                                Ingrese un número telefónico valido.
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="form-label" for="tel_oficina">Telefono Oficina:</label>
                            <input value="<?php if (isset($user)){ echo $user['tel_oficina'];} else {echo'';}?>" id="tel_oficina" class="form-control" type="text" name="tel_oficina">
                            <div class="invalid-feedback">
                                Ingrese un número telefónico valido.
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="form-label" for="extension">Extension:</label>
                            <input value="<?php if (isset($user)){ echo $user['extension'];} else {echo'';}?>" id="extension" class="form-control" type="text" name="extension">
                            <div class="invalid-feedback">
                                Extensión valida requerida.
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="form-label" for="correo_institucional">Correo Institucional:</label>
                            <input value="<?php if (isset($user)){ echo $user['correo_institucional'];} else {echo'';}?>" id="correo_institucional" class="form-control" type="email" name="correo_institucional" required=''>
                            <div class="invalid-feedback">
                                Ingrese una dirección de correo electrónico valida.
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="form-label" for="correo_personal">Correo Personal:</label>
                            <input value="<?php if (isset($user)){ echo $user['correo_personal'];} else {echo'';}?>" id="correo_personal" class="form-control" type="email" name="correo_personal" required=''>
                            <div class="invalid-feedback">
                                Ingrese una dirección de correo electrónico valida.
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="form-label" for="comision">Comision:</label>
                            <input value="<?php if (isset($user)){ echo $user['comision'];} else {echo'';}?>" id="comision" class="form-control" type="number" name="comision">
                            <div class="invalid-feedback">
                                Ingrese una comisión valida.
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="form-label" for="protesta">Protesta:</label>
                            <input value="<?php if (isset($user)){ echo $user['protesta'];} else {echo'';}?>" id="protesta" class="form-control" type="date" name="protesta" required=''>
                            <div class="invalid-feedback">
                                Ingrese una fecha valida.
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="form-label" for="consejero_asociado">Consejero Asociado:</label>
                            <input value="<?php if (isset($user)){ echo $user['consejero_asociado'];} else {echo'';}?>" id="consejero_asociado" class="form-control" type="number" name="consejero_asociado" required=''>
                            <div class="invalid-feedback">
                                Ingrese un consejero valido.
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="form-label" for="foto">Foto:</label>
                            <input value="<?php if (isset($user)){ echo $user['foto'];} else {echo'';}?>" id="foto" class="form-control" type="text" name="foto" required=''>
                        </div>
                        <p></p>
                        <button class="btn btn-success" type="submit"><?php if (isset($user)){ echo 'Guardar';} else { echo 'Crear';}?></button>
                        <a href="<?=base_url('listView/'.$estatus='3');?>" class="btn btn-primary">Regresar</a>
                    <?=form_close()?>
                </p>
            </div>
        </div>
    </div>
</section>
<script type="application/javascript">
    (function () {
        'use strict'
        // Fetch all the forms we want to apply custom Bootstrap validation styles to
        var forms = document.querySelectorAll('.needs-validation')
        // Loop over them and prevent submission
        Array.prototype.slice.call(forms)
          .forEach(function (form) {
            form.addEventListener('submit', function (event) {
              if (!form.checkValidity()) {
                event.preventDefault()
                event.stopPropagation()
              }

              form.classList.add('was-validated')
            }, false)
          })
    })();

    $(document).ready( function () {
        $('#tel_cel_1').inputmask("(999) 999-9999");
        $('#tel_casa').inputmask("(999) 999-9999");
        $('#tel_oficina').inputmask("(999) 999-9999");
        $('#tel_cel_2').inputmask("(999) 999-9999");
    } );
</script>
<?=$footer?>