<?=$common?>
<?=$header?>
<?=$menu?>

<section class="content-header">
    <div class="container-fluid">
        <h1>
            <i class="fas fa-users"></i>
            Usuarios
        </h1>
    </div>
</section>
<section class="content">
    <div class="container-fluid">
        <div class="outer">
            <div class="inner">
                <a href="<?=base_url('processView')?>" class="btn btn-success"><i class="fas fa-plus"></i> Crear informacion de consejero</a>
            </div>
        </div>
        <p></p>
        <div class="card">
            <ul class="nav nav-tabs">
                <li class="nav-item">
                    <a class="nav-link <?php if($estado=='3'){echo 'active';} ?>" aria-current="page" href="<?php  echo base_url('listView/'.$estatus='3');?>" id="allUsersTab" data-bs-target="#tab-allUsersTable">Todos</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link <?php if($estado=='1'){echo 'active';} ?>" href="<?php  echo base_url('listView/'.$estatus='1');?>" id="activeUsersTab" data-bs-target="#tab-activeUsersTable">Activos</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link <?php if($estado=='0'){echo 'active';} ?>" href="<?php  echo base_url('listView/'.$estatus='0');?>" id="inactiveUsersTab" data-bs-target="#tab-inactiveUsersTable">Inactivos</a>
                </li>
            </ul>
            <!--<nav>
                <div class="nav nav-tabs" id="nav-tab" role="tablist">
                    <button onclick="userTable('-1')" class="nav-link active" id="allUsersTab" data-bs-toggle="tab" data-bs-target="#tab-allUsersTable" type="button" role="tab" aria-controls="nav-todos" aria-selected="true">Todos los usuarios</button>
                    <button onclick="userTable('1')" class="nav-link" id="activeUsersTab" data-bs-toggle="tab" data-bs-target="#tab-activeUsersTable" type="button" role="tab" aria-controls="nav-activo" aria-selected="false">Usuarios activos</button>
                    <button onclick="userTable('0')" class="nav-link" id="inactiveUsersTab" data-bs-toggle="tab" data-bs-target="#tab-inactiveUsersTable" type="button" role="tab" aria-controls="nav-inactivo" aria-selected="false">Usuarios inactivos</button>
                </div>
            </nav>-->
            <div class="tab-content bg-white" style="padding: 17px;">
                <div class="tab-pane active" id="tab-allUsersTable">
                    <table id="allUsersTable" class="table  hover" style="width:100%;">
                        <thead>
                            <th>Matrícula</th>
                            <th>Grado</th>
                            <th>Nombre</th>
                            <th>Apellido paterno</th>
                            <th>Apellido materno</th>
                            <th>Cargo</th>
                            <th>Tel Oficina</th>
                            <th>Correo Institucional</th>
                            <?php if ($estado=='3') {echo '<th>Estado</th>';}?>
                            <th>Acciones</th>
                        </thead>
                        <tbody>
                            <?php foreach($users as $user):; ?>
                            <tr>
                                <td><?= $user['id'] ?></td>
                                <td><?= $user['grado'] ?></td>
                                <td><?= $user['nombre'] ?></td>
                                <td><?= $user['apellido_paterno'] ?></td>
                                <td><?= $user['apellido_materno'] ?></td>
                                <td><?php if ($user['cargo']=='1'){echo 'Director';}else if ($user['cargo']=='2'){echo 'Docente';}else if ($user['cargo']=='3'){echo 'Alumno';}else if ($user['cargo']=='4'){echo 'Adjunto';} else {echo $user['cargo'];} ?></td>
                                <td><?= $user['tel_oficina'] ?></td>
                                <td><?= $user['correo_institucional'] ?></td>
                                <?php if ($estado=='3' && $user['activo']=='1') {echo '<td>Activo</td>';} else if ($estado=='3' && $user['activo']=='0'){echo '<td>Inactivo</td>';}?>
                                <td>
                                <div class="outer" style="white-space: nowrap;">
                                    <div class="inner">
                                        <button class="btn btn-primary" type="submit" data-bs-toggle="modal" data-bs-target="#modalDetails1<?=$user['id']?>" id="btnDetails" display="inline-block"><i class="fa-solid fa-info"></i></button>
                                    </div>
                                    <?php if ($user['activo']=='0'){
                                            echo '  <div class="inner">';
                                            echo '      <button class="btn btn-success" type="submit" data-bs-toggle="modal" data-bs-target="#modalActivate1'.$user['id'].'" id="btnActivate" display="inline-block"><i class="fa-solid fa-user-check"></i></button>';
                                            echo '  </div>';
                                            echo ' </div>';
                                            echo '<div class="modal fade" id="modalActivate1'.$user['id'].'" tabindex="-1" aria-labelledby="modalActivateLabel" aria-hidden="true">';
                                            echo '<div class="modal-dialog">';
                                            echo '  <div class="modal-content">';
                                            echo '      <div class="modal-header">';
                                            echo '          <h5 class="modal-title" id="modalActivateLabel">Reactivar consejero</h5>';
                                            echo '          <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>';
                                            echo '      </div>';
                                            echo '      <div class="modal-body" style="text-align: center;">¿Está seguro de reactivar al siguiente consejero?</br></br><strong>'.$user['id'].'(Inactivo)</strong></div>';
                                            echo '      <div class="modal-footer">';
                                            echo '          <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cerrar</button>';
                                            echo '          <a href="'.base_url('activate/'.$user['id']).'" class="btn btn-primary">Confirmar</a>';
                                            echo '      </div>';
                                            echo '  </div>';
                                            echo '</div>';
                                            echo '';
                                            echo '';

                                        } else {
                                            echo '</div>';
                                        }
                                    ?>
                                    </div>
                                    <div class="modal fade" id="modalDetails1<?=$user['id']?>" tabindex="-1" aria-labelledby="modalDetailsLabel" aria-hidden="true">
                                        <div class="modal-dialog">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title" id="modalDetailsLabel">Detalles</h5>
                                                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                                </div>
                                                <div class="modal-body">
                                                    <div class="row">
                                                        <div class="row">
                                                            <div class="col-sm-5"><b>Matricula:</b></div>
                                                            <div class="col-sm-5"><?=$user['id']?></div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-sm-5"><b>Grado:</b></div>
                                                            <div class="col-sm-5"><?=$user['grado']?></div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-sm-5"><b>Nombre:</b></div>
                                                            <div class="col-sm-5"><?=$user['nombre']?></div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-sm-5"><b>Apellido paterno:</b></div>
                                                            <div class="col-sm-5"><?=$user['apellido_paterno']?></div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-sm-5"><b>Apellido materno:</b></div>
                                                            <div class="col-sm-5"><?=$user['apellido_materno']?></div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-sm-5"><b>Cumpleaños :</b></div>
                                                            <div class="col-sm-5"><?=$user['cumpleanos']?></div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-sm-5"><b>Cargo :</b></div>
                                                            <div class="col-sm-5"><?php if ($user['cargo']=='1'){echo 'Director';}else if ($user['cargo']=='2'){echo 'Docente';}else if ($user['cargo']=='3'){echo 'Alumno';}else if ($user['cargo']=='4'){echo 'Adjunto';} ?></div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-sm-5"><b>Tipo :</b></div>
                                                            <div class="col-sm-5"><?php if ($user['tipo']=='1'){echo 'Propietario';} else if ($user['tipo']=='2'){echo 'Suplente';}?></div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-sm-5"><b>DAUA :</b></div>
                                                            <div class="col-sm-5"><?=$user['daua']?></div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-sm-5"><b>Tel. celular 1 :</b></div>
                                                            <div class="col-sm-5"><?=$user['tel_cel_1']?></div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-sm-5"><b>Tel. celular 1 Whatsapp :</b></div>
                                                            <div class="col-sm-5"><?=$user['tel_cel_1_whats']?></div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-sm-5"><b>Tel. celular 2 :</b></div>
                                                            <div class="col-sm-5"><?= $user['tel_cel_2'] ?></div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-sm-5"><b>Tel. celular 2 Whatsapp :</b></div>
                                                            <div class="col-sm-5"><?=$user['tel_cel_2_whats'] ?></div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-sm-5"><b>Tel. casa :</b></div>
                                                            <div class="col-sm-5"><?=$user['tel_casa']?></div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-sm-5"><b>Tel. Oficina :</b></div>
                                                            <div class="col-sm-5"><?= $user['tel_oficina'] ?></div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-sm-5"><b>Tel. Oficina :</b></div>
                                                            <div class="col-sm-5"><?= $user['tel_oficina'] ?></div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-sm-5"><b>Extension :</b></div>
                                                            <div class="col-sm-5"><?= $user['extension'] ?></div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-sm-5"><b>Comision :</b></div>
                                                            <div class="col-sm-5"><?=$user['comision']?></div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-sm-5"><b>Protesta :</b></div>
                                                            <div class="col-sm-5"><?=$user['protesta']?></div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-sm-5"><b>Correo personal :</b></div>
                                                            <div class="col-sm-5"><?=$user['correo_personal']?></div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-sm-5"><b>Consejero asociado :</b></div>
                                                            <div class="col-sm-5"><?=$user['consejero_asociado']?></div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-sm-5"><b>Foto :</b></div>
                                                            <div class="col-sm-5"><?=$user['foto']?></div>
                                                        </div>
                                                        </div>
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cerrar</button>
                                                    <a href="<?=base_url('processView/'.$user['id'])?>" class="btn btn-success float-start">Editar informacion</a>
                                                    <?php if ($user['activo']=='1'){echo '<button  type="button" class="btn-danger btn float-start" data-bs-toggle="modal" data-bs-dismiss="modal" data-bs-target="#modalDeactivate1'.$user['id'].'" id="btnDeactivate" display="inline-block" >Dar de baja</button>';}?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="modal fade" id="modalDeactivate1<?=$user['id']?>" tabindex="-1" aria-labelledby="modalDeactivateLabel"  aria-hidden="true">
                                        <div class="modal-dialog">
                                            <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h5 class="modal-title" id="modalDeactivateLabel">Dar de baja al consejero</h5>
                                                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                                    </div>
                                                <div class="modal-body" style="text-align: center;">Está intentando dar de baja al siguiente consejero:</br></br><strong><?=$user['id']?> (Activo)</strong></div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-secondary"  data-bs-dismiss="modal">Cerrar</button>
                                                    <a href="<?=base_url('deactivate/'.$user['id'])?>" class="btn btn-primary">Confirmar</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                                <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
                <div class="tab-pane" id="tab-activeUsersTable">
                </div>
                <div class="tab-pane" id="tab-inactiveUsersTable">
                </div>
            </div>
        </div>
    </div>
</section>
<script type="application/javascript">
    $(document).ready( function () {
        $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
            $.fn.dataTable.tables({ visible: true, api: true }).columns.adjust().draw();
        });
        $('#allUsersTable').DataTable({   
            "initComplete": function (settings, json) {  
            $("#allUsersTable").wrap("<div style='overflow:auto; width:100%; position:relative;'></div>");            
          },
        });
        $("#btnDeactivate").click(function(){
            $("#modalDeactivate1").modal("show");
        });
        $("#btnActivate").click(function(){
            $("#modalActivate1").modal("show");
        });
        $("#btnDetails").click(function(){
          $("#modalDetails1").modal("show");
        });
    } );
</script>
<?=$footer?>