<?=$common?>
<?=$header?>
<?=$menu?>
<section class="content-header">
    <div class="container-fluid">
        <h1>
            <i class="fas fa-users"></i>
            Usuarios
        </h1>
    </div>
</section>
<section class="content">
    <div class="container-fluid">
        <div class="card">
            <div class="card-body">
                <h5 class="card-title">Ingrese el archivo .cvs o .xls a importar.</h5>
                <p class="card-text">
                    <?php foreach ($errors as $error): ?>
                        <li><?= esc($error) ?></li>
                    <?php endforeach ?>
                    <?=form_open_multipart('fileUpload','class="needs-validation" novalidate')?>
                        <div class="form-group">
                            <label class="form-label" for="fileForm">Archivo:</label>
                            <input type="file" name="userfile" class="form-control" id="file" required=''>
                            <div class="invalid-feedback">
                                Ingrese un archivo valido.
                            </div>
                        </div>
                        <div class="outer">
                            <div class="inner">
                                <div class="form-group">
                                    <button class="btn btn-success" type="submit"><i class="fa-solid fa-upload"></i> Subir archivo</button>
                                </div>
                            </div>
                            <div class="inner">
                                <a href="<?=base_url('fetchLoadUsersView')?>" class="btn btn-primary">Regresar</a>
                            </div>
                        </div>
                    <?=form_close()?>
                </p>
            </div>
        </div>
    </div>
</section>
<script type="application/javascript">
    (function () {
        'use strict'
        // Fetch all the forms we want to apply custom Bootstrap validation styles to
        var forms = document.querySelectorAll('.needs-validation')
        // Loop over them and prevent submission
        Array.prototype.slice.call(forms)
          .forEach(function (form) {
            form.addEventListener('submit', function (event) {
              if (!form.checkValidity()) {
                event.preventDefault()
                event.stopPropagation()
              }

              form.classList.add('was-validated')
            }, false)
          })
    })();
</script>
<?=$footer?>