<?=$common?>
<?=$header?>
<?=$menu?>
<style>
    #scroll-div {
        height: 150px;
        border: 1px solid #ddd;
        
        overflow-y: scroll;
        border-color: #ed969e;
        background-color: #f5c6cb;
    }
</style>
<section class="content-header">
    <div class="container-fluid">
        <h1>
            <i class="fas fa-users"></i>
            Usuarios
            
        </h1>
    </div>
</section>
<section class="content">
    <div class="container-fluid">
        <div class="outer">
            <div class="inner">
                <a href="<?=base_url('uploadView')?>" class="btn btn-primary"><i class="fa-solid fa-file"></i> Subir consejeros desde Excel</a>
            </div>
            <div class="inner">
                <button onclick="listErrors()" type="button" class="btn btn-success" data-bs-toggle="modal" data-bs-target="#errorsModal">
                <i class="fa-solid fa-database"></i> Verificar estructura del consejo
                </button>
            </div>
        </div>
        <p></p>
        <div class="card">        
            <div class="card-body">
                <p class="card-text" id='errors'>
                    <div class="table-responsive">
                        <?php
                            if (session('errors')!=null){
                                $aux = session('errors');
                                $string = '';
                                $string = '<div class="table-responsive">
                                        <table class="table align-middle table-info">
                                            <tr>
                                                <thead>
                                                    <th class="text-center">Tipo de error</th>
                                                    <th class="text-center">Total</th>
                                                    <th class="text-center">Descripcion</th>
                                                </thead>
                                            </tr>
                                            <tbody>
                                                <tr id="associated_advisors" class="table-danger">
                                                    <td>Consejero asociado</td>
                                                    <td class="text-center">'.count($aux['associated_advisors']).'</td>
                                                    <td>';
                                                    foreach ($aux['associated_advisors'] as $associated_advisor){
                                                        $string .= '<a href="'.base_url('processLoadUsersView/'.$associated_advisor).'">'.$associated_advisor.'<br></a>';
                                                    }
                                $string .= '
                                                    </td>
                                                </tr>
                                                <tr id="duplicated_advisors" class="table-danger">
                                                    <td>Consejero duplicado</td>
                                                    <td class="text-center">'.count($aux['advisor_duplicity']).'</td>
                                                    <td>';
                                                        foreach ($aux['advisor_duplicity'] as $duplicated_advisor){
                                                            $string .= '<a href="'.base_url('processLoadUsersView/'.$duplicated_advisor['id']).'">'.$duplicated_advisor['nombre'].'<br></a>';
                                                        }
                                $string .= '
                                                    </td>
                                                </tr>
                                                <tr id="missing_advisors" class="table-danger">
                                                    <td>Faltan consejeros</td>
                                                    <td class="text-center">'.count($aux['missing_advisors']).'</td>
                                                    <td>
                                                        <div id="scroll-div"  class="table-danger">';
                                                            foreach ($aux['missing_advisors'] as $advisor){
                                                                $string .= '<a>Faltan '.$advisor['num_adv']. ' consejeros en la unidad ' .$advisor['nombre'].'<br></a>';
                                                            }
                                $string .= '
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr id="missing_teachers" class="table-danger">
                                                    <td>Faltan docentes</td>
                                                    <td class="text-center">'.count($aux['missing_teachers']).'</td>
                                                    <td>
                                                        <div id="scroll-div"  class="table-danger">';
                                                            foreach ($aux['missing_teachers'] as $teacher){
                                                                $string .= '<a>Faltan '.$teacher['num_adv']. ' docentes en la unidad ' .$teacher['nombre'].'<br></a>';
                                                            }
                                                                
                                $string .= '                            
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr id="missing_director" class="table-danger">
                                                    <td>Falta director</td>
                                                    <td class="text-center">'.count($aux['missing_director']).'</td>
                                                    <td>
                                                        <div id="scroll-div"  class="table-danger">';
                                                            foreach ($aux['missing_director'] as $director){
                                                                $string .= '<a>Faltan registrar el director en la unidad ' .$director['nombre'].'<br></a>';
                                                            }
                                $string .= '
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr id="missing_director" class="table-danger">
                                                    <td>Faltan alumnos</td>
                                                    <td class="text-center">'.count($aux['missing_students']).'</td>
                                                    <td>
                                                        <div id="scroll-div"  class="table-danger">';
                                                            foreach ($aux['missing_students'] as $student){
                                                                $string .= '<a>Faltan '.$student['num_adv']. ' estudiantes en la unidad ' .$student['nombre'].'<br></a>';
                                                            }
                                $string .= '
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr id="missing_director" class="table-danger">
                                                    <td>Faltan adjuntos</td>
                                                    <td class="text-center">'.count($aux['missing_adjoints']).'</td>
                                                    <td>';
                                                    foreach ($aux['missing_adjoints'] as $adjoint){
                                                        $string .= '<a>Faltan '.$adjoint['num_adv']. ' adjuntos en la unidad ' .$adjoint['nombre'].'<br></a>';
                                                    }
                                $string .= '    
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                ';

                                echo $string;
                            }
                        ?>
                    </div>
                </p>
                <ul class="nav nav-tabs">
                <li class="nav-item">
                    <a class="nav-link active" aria-current="page" href="<?php  echo base_url('fetchLoadUsersView');?>" id="allLoadedUsersTab" data-bs-target="#tab-allLoadedUsersTable">Todos</a>
                </li>
            </ul>
            <div class="tab-content bg-white" style="padding: 17px;">
                <div class="tab-pane active" id="tab-allLoadedUsersTable">
                    <table id="allLoadedUsersTable" class="table  hover" style="width:100%;">
                        <thead>
                            <th>Orden</th>
                            <th>DAUA</th>
                            <th>Curul</th>
                            <th>ID</th>
                            <th>Consejero asociado</th>
                            <th>Grado</th>
                            <th>Nombre</th>
                            <th>Apellido paterno</th>
                            <th>Apellido materno</th>
                            <th>Cargo</th>
                            <th>Tipo</th>
                            <th>Correo Institucional</th>
                            <th>Estado</th>
                            <th>Acciones</th>
                        </thead>
                        <tbody id="loadedUsersTbody">
                        </tbody>
                    </table>
                </div>
            </div>
            </div>
        </div>
    </div>

<!-- Errors Modal -->
<div class="modal fade" id="errorsModal" tabindex="-1" aria-labelledby="errorsModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg modal-dialog-scrollable">
    <div class="modal-content">
      <div class="modal-header">
        <h1 class="modal-title fs-5" id="errorsModalLabel">Errores en la estructura de la base de datos</h1>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
        <div class="container-fluid">
            <div class="table-responsive">
                <table class="table align-middle table-info">
                    <tr>
                        <thead>
                            <th class="text-center">Tipo de error</th>
                            <th class="text-center">Total</th>
                            <th class="text-center">Descripcion</th>
                        </thead>
                    </tr>
                    <tbody id="error-table-content">
                    </tbody>
                </table>
            </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>

</section>
<script type="application/javascript">
    async function listErrors(){
        let url = 'verifyStructure';
        let obj;
        try{
            const res = await fetch(url);
            obj = await res.json();

            console.log(obj);
            document.getElementById('error-table-content').innerHTML = drawErrors(obj);
        } catch(error){
            console.log(error);
        }
    }
    
    function drawErrors(obj){
        let associated_errors = ''
        let duplicity_errors = ''
        let num_advisors = ''
        let num_teacher = ''
        let num_student = ''
        let num_director = ''
        let num_adjoint = ''
        obj.associated_advisors.forEach(function(associated_advisor){
            associated_errors +=`<a href="<?=base_url('processLoadUsersView')?>${'/'+associated_advisor}">${associated_advisor}</a><br>`
        })
        obj.advisor_duplicity.forEach(function(advisor){
            duplicity_errors +=`<a href="<?=base_url('processLoadUsersView')?>${'/'+advisor.id}">${advisor.nombre}</a><br>`
        })
        obj.missing_advisors.forEach(function(advisor){
            num_advisors += `Faltan ${advisor.num_adv} consejeros en la unidad ${advisor.nombre}<br>`
        })
        obj.missing_teachers.forEach(function(teacher){
            num_teacher += `Faltan ${teacher.num_adv} docentes en la unidad ${teacher.nombre}<br>`
        })
        obj.missing_director.forEach(function(director){
            num_director += `Falta registrar al director de la unidad ${director.nombre}<br>`
        })
        obj.missing_students.forEach(function(student){
            num_student += `Faltan ${student.num_adv} estudiantes en la unidad ${student.nombre}<br>`
        })
        obj.missing_adjoints.forEach(function(adjoint){
            num_adjoint += `Faltan ${adjoint.num_adv} adjuntos en la unidad ${adjoint.nombre}<br>`
        })
        let errors = ''
        errors+= `<tr id="associated_advisors" class="table-danger">
                    <td>Consejero asociado</td>
                    <td class="text-center">${Object.keys(obj.associated_advisors).length}</td>
                    <td>`
                    + associated_errors +
                `</td>
                </tr>
                <tr id="duplicated_advisors" class="table-danger">
                    <td>Consejero duplicado</td>
                    <td class="text-center">${Object.keys(obj.advisor_duplicity).length}</td>
                    <td>`
                        + duplicity_errors +
                    `</td>
                </tr>
                <tr id="missing_advisors" class="table-danger">
                    <td>Faltan consejeros</td>
                    <td class="text-center">${Object.keys(obj.missing_advisors).length}</td>
                    <td>
                        <div id="scroll-div"  class="table-danger">`
                        + num_advisors +
                        `</div>
                    </td>
                </tr>
                <tr id="missing_teachers" class="table-danger">
                    <td>Faltan docentes</td>
                    <td class="text-center">${Object.keys(obj.missing_teachers).length}</td>
                    <td>
                        <div id="scroll-div"  class="table-danger">`
                            + num_teacher +
                        `</div>
                    </td>
                </tr>
                <tr id="missing_director" class="table-danger">
                    <td>Falta director</td>
                        <td class="text-center">${Object.keys(obj.missing_director).length}</td>
                        <td>
                        <div id="scroll-div"  class="table-danger">`
                            + num_director +
                        `
                        </div>
                    </td>
                </tr>
                <tr id="missing_director" class="table-danger">
                    <td>Faltan alumnos</td>
                    <td class="text-center">${Object.keys(obj.missing_students).length}</td>
                    <td>
                    <div id="scroll-div"  class="table-danger">`
                        + num_student+
                        `
                        </div>
                    </td>    
                </tr>
                <tr id="missing_director" class="table-danger">
                    <td>Faltan adjuntos</td>
                    <td class="text-center">${Object.keys(obj.missing_adjoints).length}</td>
                    <td>`
                        + num_adjoint +
                        `
                    </td>
                </tr>`
        return errors
    }

    async function listLoadedUsers(){
        let url= 'fetchLoadUsersView/fetchLoadedUsers';
        let obj;
        try{
            const res = await fetch(url);
            obj = await res.json();

            document.getElementById('loadedUsersTbody').innerHTML = tableContent(obj);

            $('#allLoadedUsersTable').DataTable({   
                "initComplete": function (settings, json) {  
                    $("#allLoadedUsersTable").wrap("<div style='overflow:auto; width:100%; position:relative;'></div>");            
                },
            });
        } catch (error){
            console.log(error);
        }
    }

    function tableContent(obj){
        let table = ''
        aux = 0;
        obj.users.forEach(function(user){
            switch (user.activo){
                case '0':
                    user.activo ='Inactivo'
                    break
                case '1':
                    user.activo ='Activo'
                    break
            }
            switch (user.cargo){
                case '1':
                    user.cargo = 'Director'
                    break
                case '2':
                    user.cargo = 'Docente'
                    break    
                case '3':
                    user.cargo = 'Alumno'
                    break
                case '4':
                    user.cargo = 'Adjunto'
                    break
            }
            switch (user.tipo){
                case '1':
                    user.tipo ='Propietario'
                    break
                case '2':
                    user.tipo ='Suplente'
                    break
            }
            table += `<tr>
                <td>${aux}</td>
                <td>${user.daua}</td>
                <td>${user.curul}</td>
                <td>${user.id}</td>
                <td>${user.consejero_asociado}</td>
                <td>${user.grado}</td>
                <td>${user.nombre}</td>
                <td>${user.apellido_paterno}</td>
                <td>${user.apellido_materno}</td>
                <td>${user.cargo}</td>
                <td>${user.tipo}</td>
                <td>${user.correo_institucional}</td>
                <td>${user.activo}</td>
                <td>
                    <div class="outer" style="white-space: nowrap;">
                        <div class="inner">
                            <button class="btn btn-primary" type="submit" data-bs-toggle="modal" data-bs-target="#modalDetails1${user.id}" id="btnDetails" display="inline-block"><i class="fa-solid fa-info"></i></button>
                        </div>
                    </div>
                    <div class="modal fade" id="modalDetails1${user.id}" tabindex="-1" aria-labelledby="modalDetailsLabel" aria-hidden="true">
                        <div class="modal-dialog modal-lg">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="modalDetailsLabel">Detalles</h5>
                                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                </div>
                                <div class="modal-body">
                                    <div class="container-fluid "> 
                                    <div class="row">
                                        <div class="col-5" style="background-image: url(<?=base_url('public/img/login_background.png')?>);">
                                            <br><br><br><br>
                                            <div class="row">
                                                <div class="col">
                                                    <img src="<?=base_url('public/img/fotos')?>${'/'+user.foto}" width="300" height="300" class="rounded-circle img-fluid mx-auto d-block">
                                                </div>
                                            </div>
                                            <br>
                                            <div class="row">
                                                <p style="text-align: center;color:white;"><b>${user.grado} ${user.nombre} ${user.apellido_paterno} ${user.apellido_materno}</b></p>
                                            </div>
                                            <div class="row ">
                                                <div class="hstack justify-content-md-center">
                                                    <div class="p-2" style="color:white;"><b>${user.cargo}</b></div>
                                                    <div class="vr" style="color:white;"></div>
                                                    <div class="p-2" style="color:white;"><b>${user.tipo}</b></div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-1" style="flex: 0 0 8.333333%;max-width: 0.333333%;"></div>
                                        <div class="col">
                                            <br>
                                            <div class="row"><b>Información personal</b></div>
                                            <div class="row">
                                                <hr style="width:90%;text-align:left;margin-left:0;height:2px;border-width:0;color:gray;background-color:gray">
                                            </div>
                                            <div class="row">
                                                <div class="col-7"><b>ID</b></div>
                                                <div class="col"><b>Cumpleaños</b></div>
                                            </div>
                                            <div class="row">
                                                <div class="col-7">${user.id}</div>
                                                <div class="col">${user.cumpleanos}</div>
                                            </div>
                                            <br>
                                            <div class="row">
                                                <div class="col-7"><b>Protesta</b></div>
                                                <div class="col"><b>DAUA</b></div>
                                            </div>
                                            <div class="row">
                                                <div class="col-7">${user.protesta}</div>
                                                <div class="col">${user.daua}</div>
                                            </div>
                                            <br>
                                            <div class="row">
                                                <div class="col-7"><b>Consejero asociado</b></div>
                                                <div class="col"><b>Comision</b></div>
                                            </div>
                                            <div class="row">
                                                <div class="col-7">${user.consejero_asociado}</div>
                                                <div class="col">${user.comision}</div>
                                            </div>

                                            <br>
                                            <div class="row"><b>Información de contacto</b></div>
                                            <div class="row">
                                                <hr style="width:90%;text-align:left;margin-left:0;height:2px;border-width:0;color:gray;background-color:gray">
                                            </div>
                                            <div class="row">
                                                <div class="col-7"><b>Correo institucional</b></div>
                                                <div class="col"><b>Tel. Oficina</b></div>
                                            </div>
                                            <div class="row">
                                                <div class="col-7">${user.correo_institucional}</div>
                                                <div class="col">${user.tel_oficina}</div>
                                            </div>
                                            <br>
                                            <div class="row">
                                                <div class="col-7"><b>Correo personal</b></div>
                                                <div class="col"><b>Extension</b></div>
                                            </div>
                                            <div class="row">
                                                <div class="col-7">${user.correo_personal}</div>
                                                <div class="col">${user.extension}</div>
                                            </div>
                                            <br>
                                            <div class="row">
                                                <div class="col-7"><b>Tel. Cel. 1</b></div>
                                                <div class="col"><b>Tel. Cel. 2</b></div>
                                            </div>
                                            <div class="row">
                                                <div class="col-7">${user.tel_cel_1}</div>
                                                <div class="col">${user.tel_cel_2}</div>
                                            </div>
                                            <br>
                                            <div class="row">
                                                <div class="col-7"><b>Tel. Casa</b></div>
                                            </div>
                                            <div class="row">
                                                <div class="col-7">${user.tel_casa}</div>
                                            </div>
                                            <br>
                                        </div>
                                    </div>
                                    </div> 
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cerrar</button>
                                    <a href="<?=base_url('processLoadUsersView')?>${'/'+user.id}" class="btn btn-success float-start">Editar informacion</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </td>
            </tr>`
            aux++
        })
        return table;
    }

    listLoadedUsers()

    
        $('#displayNoneA1').click(function(e) {
            if( $('#hide-meA1').is(":visible") ) {
                $('#hide-meA1').css('display', 'none'); 
            } else {
                $('#hide-meA1').css('display', 'block');
            }
        });
    
    
</script>
<?=$footer?>