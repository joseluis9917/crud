<?=$common?>
<?=$header?>
<?=$menu?>
<section class="content-header">
    <div class="container-fluid">
        <h1>
            <i class="fas fa-users"></i>
            Usuarios
        </h1>
    </div>
</section>
<section class="content">
    <div class="container-fluid">
        <div class="card">
            <div class="card-body">
                <h5 class="card-title">Archivo ingresado correctamente.</h5>
                    <table id="excelTable" class="table hover" style=" width:100%;">
                        <thead>
                        <th>Curul</th>
                        <th>Id</th>
                        <th>Consejero Asociado</th>
                        <th>DAUA</th>
                        <th>Grado</th>
                        <th>Nombre</th>
                        <th>Apellido paterno</th>
                        <th>Apellido materno</th>
                        <th>Cumpleaños</th>
                        <th>Cargo</th>
                        <th>Tipo</th>
                        <th>Tel Cel 1</th>
                        <th>Tel Cel 1 Whats</th>
                        <th>Tel Cel 2</th>
                        <th>Tel Cel 2 Whats</th>
                        <th>Tel Casa</th>
                        <th>Tel Oficina</th>
                        <th>Extension</th>
                        <th>Correo Institucional</th>
                        <th>Correo Personal</th>
                        <th>Comision</th>
                        <th>Protesta</th>
                        <th>Activo</th>
                        <th>Foto</th>
                        </thead>
                        <tbody>
                            <?php foreach($table as $user):; ?>
                                <tr>
                                    <td><?= $user['curul'] ?></td>
                                    <td><?= $user['id'] ?></td>
                                    <td><?= $user['consejero_asociado'] ?></td>
                                    <td><?= $user['daua'] ?></td>
                                    <td><?= $user['grado'] ?></td>
                                    <td><?= $user['nombre'] ?></td>
                                    <td><?= $user['apellido_paterno'] ?></td>
                                    <td><?= $user['apellido_materno'] ?></td>
                                    <td><?= $user['cumpleanos'] ?></td>
                                    <td><?= $user['cargo'] ?></td>
                                    <td><?= $user['tipo'] ?></td>
                                    <td><?= $user['tel_cel_1'] ?></td>
                                    <td><?= $user['tel_cel_1_whats'] ?></td>
                                    <td><?= $user['tel_cel_2'] ?></td>
                                    <td><?= $user['tel_cel_2_whats'] ?></td>
                                    <td><?= $user['tel_casa'] ?></td>
                                    <td><?= $user['tel_oficina'] ?></td>
                                    <td><?= $user['extension'] ?></td>
                                    <td><?= $user['correo_institucional'] ?></td>
                                    <td><?= $user['correo_personal'] ?></td>
                                    <td><?= $user['comision'] ?></td>
                                    <td><?= $user['protesta'] ?></td>
                                    <td><?= $user['activo'] ?></td>
                                    <td><?= $user['foto'] ?></td>
                                </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                <p class="card-text">
                    <div class="outer">
                        <div class="inner">
                            <a href="<?php echo base_url('fetchLoadUsersView'); ?>" class="btn btn-primary">Regresar</a>
                        </div>
                        <div class="inner">
                            <?=form_open('uploadToDatabase')?>
                                <input value="<?=$filename?>" id="filename" class="form-control" type="hidden" name="filename">
                                <button type="submit" class="btn btn-success">Guardar en la base de datos</button>
                            <?=form_close()?>
                        </div>
                    </div>
                </p>
            </div>
        </div>
    </div>
</section>
<script type="application/javascript">
    $(document).ready( function () {
        $('#excelTable').DataTable({   
            "initComplete": function (settings, json) {  
            $("#excelTable").wrap("<div style='overflow:auto; width:100%; position:relative;'></div>");            
          },
        });
    } );
</script>
<?=$footer?>