<?=$common?>
<?=$header?>
<?=$menu?>
<section class="content-header">
    <div class="container-fluid">
        <h1>
            <i class="fas fa-clipboard-user"></i>
            Asistencia
        </h1>
    </div>
</section>
<section class="content">
    <div class="container-fluid">
        <div class="card">
            <div class="card-body">
                <h5 class="card-title">Número de asistentes.</h5>
                    <p id='attendance'></p>
            </div>
        </div>
    </div>
        

</section>

<script>
    async function registers(){
        let url= 'fetch-attendance/fetchRegisters';
        let obj;
        try{
            const res = await fetch(url);
            obj = await res.json();
            console.log(obj);
            document.getElementById('attendance').innerHTML = '<br>sesion[1]='+obj.sesion;
            
        } catch (error){
            console.log(error);
        }
        setTimeout(registers, 30000);
    }
    registers()
</script>

<?=$footer?>