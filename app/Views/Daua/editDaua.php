<?=$common?>
<?=$header?>
<?=$menu?>
<section class="content-header">
    <div class="container-fluid">
        <h1>
            <i class="fas fa-building"></i>
            DAUA
        </h1>
    </div>
</section>
<section class="content">
    <div class="container-fluid">
        <div class="card">
            <div class="card-body">
                <h5 class="card-title">Ingresar datos de la unidad:</h5>
                <p class="card-text">
                    <?php foreach ($errors as $error): ?>
                        <li><?= esc($error) ?></li>
                    <?php endforeach ?>
                    <?php  
                        if (isset($daua)){
                                echo form_open('processDaua/'.$daua['id_daua'], 'class="needs-validation" novalidate');
                            } 
                            else {
                                echo form_open('processDaua','class="needs-validation" novalidate');
                            }
                    ?>
                        <div class="form-group">
                            <label class="form-label" for="nombre">Nombre:</label>
                            <input value="<?php if (isset($daua)){ echo $daua['nombre'];} else { echo'';}?>" id="nombre" class="form-control" type="text" name="nombre" required=''>
                            <div class="invalid-feedback">
                                Nombre valido es requerido.
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="form-label" for="clave">Clave:</label>
                            <input value="<?php if (isset($daua)){ echo $daua['clave'];} else { echo'';}?>" id="clave" class="form-control" type="text" name="clave" required=''>
                            <div class="invalid-feedback">
                                Clave valida requerida.
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="form-label" for="clave_coll">Clave Coll.:</label>
                            <input value="<?php if (isset($daua)){ echo $daua['clave_coll'];} else { echo'';}?>" id="clave_coll" class="form-control" type="text" name="clave_coll" required=''>
                            <div class="invalid-feedback">
                                Clave Coll. valida requerida.
                            </div>
                        </div>
                        <p></p>
                        <button class="btn btn-success" type="submit"><?php if (isset($daua)){ echo 'Guardar';} else { echo 'Crear';}?></button>
                        <a href="<?=base_url('daua/'.$status='3')?>" class="btn btn-primary">Regresar</a>
                    <?=form_close()?>
                </p>
            </div>
        </div>
    </div>
</section>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.inputmask/5.0.7/jquery.inputmask.min.js"></script>
<script type="application/javascript">
    (function () {
        'use strict'
        // Fetch all the forms we want to apply custom Bootstrap validation styles to
        var forms = document.querySelectorAll('.needs-validation')
        // Loop over them and prevent submission
        Array.prototype.slice.call(forms)
          .forEach(function (form) {
            form.addEventListener('submit', function (event) {
              if (!form.checkValidity()) {
                event.preventDefault()
                event.stopPropagation()
              }

              form.classList.add('was-validated')
            }, false)
          })
    })();
</script>
<?=$footer?>