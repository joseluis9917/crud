<?=$common?>
<?=$header?>
<?=$menu?>

<section class="content-header">
    <div class="container-fluid">
        <h1>
            <i class="fas fa-building"></i>
            DAUA
        </h1>
    </div>
</section>
<section class="content">
    <div class="container-fluid">
        <div class="card">
            <div class="card-body">
                <h3 class="card-title text-center">Detalles</h3>
                <br>
                    <div class="row">
                        <div class="row">
                            <div class="col-sm-4">
                                <b>Nombre:</b>
                            </div>
                            <div class="col-sm-4"><?=$daua['nombre']?></div>
                        </div>
                        <div class="row">
                            <div class="col-sm-4">
                                <b>Clave:</b>
                            </div>
                            <div class="col-sm-4"><?=$daua['clave']?></div>
                        </div>
                        <div class="row">
                            <div class="col-sm-4">
                                <b>Clave Coll. :</b>
                            </div>
                            <div class="col-sm-4"><?=$daua['clave_coll']?></div>
                        </div>
                    </div>
                <p></p>
                <p>
                    <div class="outer">
                        <div class="inner">
                            <a href="<?=base_url('daua')?>" class="btn btn-primary float-start">Regresar</a>
                        </div>
                        <div class="inner">
                            <a href="<?=base_url('processDauaView/'.$daua['id_daua'])?>" class="btn btn-success float-start">Editar informacion</a>
                        </div>
                        <div class="inner">
                            <button  type="button" class="btn-danger btn float-start" data-bs-toggle="modal" data-bs-target="#modalDeactivateDaua<?=$daua['id_daua']?>" id="btnDeactivateDaua" display="inline-block" <?php if($daua['activo']=='0'){echo 'style="display: none;"';}?>>Dar de baja</button>
                        </div>
                    </div>
                    <div class="modal fade" id="modalDeactivateDaua<?=$daua['id_daua']?>" tabindex="-1" aria-labelledby="modalDeactivateDauaLabel" aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="modalDeactivateDauaLabel">Desactivar unidad</h5>
                                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                </div>
                                <div class="modal-body" style="text-align: center;">¿Está seguro de desactivar la siguiente unidad?</br></br><strong><?=$daua['nombre']?> (Activa)</strong></div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cerrar</button>  
                                    <a href="<?=base_url('deactivateDaua/'.$daua['id_daua'])?>" class="btn btn-primary">Confirmar</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </p>
            </div>
        </div>
    </div>
</section>

<?=$footer?>