<?=$common?>
<?=$header?>
<?=$menu?>
<section class="content-header">
    <div class="container-fluid">
        <h1>
            <i class="fas fa-building"></i>
            DAUA
        </h1>
    </div>
</section>
<section class="content">
    <div class="container-fluid">
        <div class="outer">
            <div class="inner">
                <a href="<?=base_url('processDauaView')?>" class="btn btn-success"><i class="fas fa-plus"></i> Crear unidad</a>
            </div>
        </div>
        <p></p>
        <div class="card">
            <ul id="daua-tabs" class="nav nav-tabs" >
                <li class="nav-item">
                    <a class="nav-link <?php if($estado=='3'){echo 'active';} ?>" aria-current="page" href="<?php  echo base_url('daua/'.$status='3');?>" data-toggle="tab" id="allDauaTab" >Todas las unidades</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link <?php if($estado=='1'){echo 'active';} ?>" href="<?php  echo base_url('daua/'.$status='1');?>"  id="activeDauaTab" >Unidades activas</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link <?php if($estado=='0'){echo 'active';} ?>" href="<?php  echo base_url('daua/'.$status='0');?>"  id="inactiveDauaTab" >Unidades inactivas</a>
                </li>
            </ul>
                <!--<nav>
                    <div class="nav nav-tabs" id="nav-tab" role="tablist">
                        <button class="nav-link active" id="allDauaTab" data-bs-toggle="tab" data-bs-target="#tab-allDauaTable1" type="button" role="tab" aria-controls="nav-todos" aria-selected="true">Todas las unidades</button>
                        <button class="nav-link" id="activeDauaTab" data-bs-toggle="tab" data-bs-target="#tab-allDauaTable2" type="button" role="tab" aria-controls="nav-activo" aria-selected="false">Unidades activas</button>
                        <button class="nav-link" id="inactiveDauaTab" data-bs-toggle="tab" data-bs-target="#tab-allDauaTable3" type="button" role="tab" aria-controls="nav-inactivo" aria-selected="false">Unidades inactivas</button>
                    </div>
                </nav>-->
            <div class="tab-content bg-white" style="padding: 17px;">
                <div class="tab-pane active" id="tab-allDauaTable1" >
                    <table id="allDauaTable1" class="table hover" style=" width:100%;">
                        <thead>
                            <th>Nombre</th>
                            <?php if ($estado=='3') {echo '<th>Estado</th>';}?>
                            <th>Clave</th>
                            <th>Clave Coll.</th>
                            <th>Acciones</th>
                        </thead>
                        <tbody>
                            <?php foreach($dauaAll as $directionAll):; ?>
                            <tr>
                                <td><?=$directionAll['nombre'] ?></td>
                                <?php if ($estado=='3' && $directionAll['activo']=='1') {echo '<td>Activo</td>';} else if ($estado=='3' && $directionAll['activo']=='0'){echo '<td>Inactivo</td>';}?>
                                <td><?= $directionAll['clave'] ?></td>
                                <td><?= $directionAll['clave_coll'] ?></td>
                                <td>
                                    <div class="outer" style="white-space: nowrap;">
                                        <div class="inner">
                                            <button class="btn btn-primary" type="submit" data-bs-toggle="modal" data-bs-target="#modalDetailsDaua1<?=$directionAll['id_daua']?>" id="btnDetailsDaua" display="inline-block"><i class="fa-solid fa-info"></i></button>
                                        </div>
                            <?php if ($directionAll['activo']=='0'){
                                echo '  <div class="inner">';
                                echo '      <button class="btn btn-success" type="submit" data-bs-toggle="modal" data-bs-target="#modalActivateDaua1'.$directionAll['id_daua'].'" id="btnActivateDaua" display="inline-block"><i class="fa-solid fa-user-check"></i></button>';
                                echo '  </div>';
                                echo '</div>';
                                echo '<div class="modal fade" id="modalActivateDaua1'.$directionAll['id_daua'].'" tabindex="-1" aria-labelledby="modalActivateDauaLabel" aria-hidden="true">';
                                echo '<div class="modal-dialog">';
                                echo '  <div class="modal-content">';
                                echo '      <div class="modal-header">';
                                echo '          <h5 class="modal-title" id="modalActivateDauaLabel">Reactivar unidad</h5>';
                                echo '          <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>';
                                echo '      </div>';
                                echo '      <div class="modal-body" style="text-align: center;">¿Está seguro de reactivar la siguiente unidad?</br></br><strong>'.$directionAll['nombre'].'(Inactiva)</strong></div>';
                                echo '      <div class="modal-footer">';
                                echo '          <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cerrar</button>';
                                echo '          <a href="'.base_url('activateDaua/'.$directionAll['id_daua']).'" class="btn btn-primary">Confirmar</a>';
                                echo '      </div>';
                                echo '  </div>';
                                echo '</div>';
                                } else {
                                echo '</div>';
                                }
                            ?>
                                    <div class="modal fade" id="modalDetailsDaua1<?=$directionAll['id_daua']?>" tabindex="-1" aria-labelledby="modalDetailsDauaLabel" aria-hidden="true">
                                        <div class="modal-dialog">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title" id="modalDetailsDauaLabel">Detalles</h5>
                                                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                                </div>
                                                <div class="modal-body">
                                                    <div class="row">
                                                        <div class="row">
                                                            <div class="col-sm-4"><b>Nombre:</b></div>
                                                            <div class="col-sm-5"><?=$directionAll['nombre']?></div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-sm-4"><b>Clave:</b></div>
                                                            <div class="col-sm-5"><?=$directionAll['clave']?></div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-sm-4"><b>Clave Coll. :</b></div>
                                                            <div class="col-sm-5"><?=$directionAll['clave_coll']?></div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cerrar</button>
                                                    <a href="<?=base_url('processDauaView/'.$directionAll['id_daua'])?>" class="btn btn-success float-start">Editar informacion</a>
                                                    <?php if ($directionAll['activo']=='1'){echo '<button  type="button" class="btn-danger btn float-start" data-bs-toggle="modal" data-bs-dismiss="modal" data-bs-target="#modalDeactivateDaua1'.$directionAll['id_daua'].'" id="btnDeactivateDaua" display="inline-block" >Dar de baja</button>';}?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="modal fade" id="modalDeactivateDaua1<?=$directionAll['id_daua']?>" tabindex="-1" aria-labelledby="modalDeactivateDauaLabel"  aria-hidden="true">             
                                        <div class="modal-dialog">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title" id="modalDeactivateDauaLabel">Desactivar unidad</h5>
                                                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                                </div>
                                                <div class="modal-body" style="text-align: center;">¿Está seguro de desactivar la siguiente unidad?</br></br><strong><?=$directionAll['nombre']?> (Activa)</strong></div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-secondary"  data-bs-dismiss="modal">Cerrar</button>
                                                    <a href="<?=base_url('deactivateDaua/'.$directionAll['id_daua'])?>" class="btn btn-primary">Confirmar</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div> 
                                </td>
                            </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>

<script type="application/javascript">
    $(document).ready( function () {
        $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
            $.fn.dataTable.tables({ visible: true, api: true }).columns.adjust().draw();
        });
        $('#allDauaTable1').DataTable({    
            "initComplete": function (settings, json) {  
            $("#allDauaTable1").wrap("<div style='overflow:auto; width:100%; position:relative;'></div>");            
          },
        });
        $("#btnDeactivateDaua").click(function(){
          $("#modalDeactivateDaua1").modal("show");
        });
        $("#btnActivateDaua").click(function(){
          $("#modalActivateDaua1").modal("show");
        });
        $("#btnDetailsDaua").click(function(){
          $("#modalDetailsDaua1").modal("show");
        });
    } );
</script>
<?=$footer?>