<?php

namespace Config;

// Create a new instance of our RouteCollection class.
$routes = Services::routes();

/*
 * --------------------------------------------------------------------
 * Router Setup
 * --------------------------------------------------------------------
 */
$routes->setDefaultNamespace('App\Controllers');
$routes->setDefaultController('Home');
$routes->setDefaultMethod('index');
$routes->setTranslateURIDashes(false);
$routes->set404Override();
// The Auto Routing (Legacy) is very dangerous. It is easy to create vulnerable apps
// where controller filters or CSRF protection are bypassed.
// If you don't want to define all routes, please use the Auto Routing (Improved).
// Set `$autoRoutesImproved` to true in `app/Config/Feature.php` and set the following to true.
// $routes->setAutoRoute(false);

/*
 * --------------------------------------------------------------------
 * Route Definitions
 * --------------------------------------------------------------------
 */

// We get a performance increase by specifying the default
// route since we don't have to scan directories.
$routes->get('/', 'AdvisorUsersInfo::index');

/*
 * --------------------------------------------------------------------
 * Additional Routing
 * --------------------------------------------------------------------
 *
 * There will often be times that you need additional routing and you
 * need it to be able to override any defaults in this file. Environment
 * based routes is one such time. require() additional route files here
 * to make that happen.
 *
 * You will have access to the $routes object within that file without
 * needing to reload it.
 */
if (is_file(APPPATH . 'Config/' . ENVIRONMENT . '/Routes.php')) {
    require APPPATH . 'Config/' . ENVIRONMENT . '/Routes.p
    hp';
}
/* Advisor Users */
$routes->get('listView/(:num)','AdvisorUsersInfo::index/$1');

$routes->get('deactivate/(:num)', 'AdvisorUsersInfo::deactivate/$1');
$routes->get('activate/(:num)', 'AdvisorUsersInfo::activate/$1');

$routes->post('process', 'AdvisorUsersInfo::process');
$routes->post('process/(:num)', 'AdvisorUsersInfo::process/$1');
$routes->get('processView/(:num)', 'AdvisorUsersInfo::process/$1');
$routes->get('processView', 'AdvisorUsersInfo::process');

/* DAUA */
$routes->get('daua/(:num)','DauaController::indexDaua/$1');

$routes->post('processDaua', 'DauaController::processDaua');
$routes->post('processDaua/(:num)', 'DauaController::processDaua/$1');
$routes->get('processDauaView/(:num)', 'DauaController::processDaua/$1');
$routes->get('processDauaView', 'DauaController::processDaua');

$routes->get('deactivateDaua/(:num)', 'DauaController::deactivateDaua/$1');
$routes->get('activateDaua/(:num)', 'DauaController::activateDaua/$1');

/* Users loaded from file */
$routes->post('uploadToDatabase', 'LoadUsersInfo::uploadToDatabase');
$routes->get('verifyStructure', 'LoadUsersInfo::verifyStructure');

$routes->get('uploadView', 'LoadUsersInfo::upload');
$routes->post('fileUpload', 'LoadUsersInfo::fileUpload');
$routes->get('fetchLoadUsersView','LoadUsersInfo::indexLoadUsers');
$routes->get('fetchLoadUsersView/fetchLoadedUsers','LoadUsersInfo::fetchLoadedUsers');
$routes->get('processLoadUsersView', 'LoadUsersInfo::processLoadedUser/$1');
$routes->get('processLoadUsersView/(:num)', 'LoadUsersInfo::processLoadedUser/$1');
$routes->post('processLoadedUser/(:num)', 'LoadUsersInfo::processLoadedUser/$1');
$routes->post('processLoadedUser', 'LoadUsersInfo::processLoadedUser');

/* Attendance  */
$routes->get('fetch-attendance', 'SessionAttendanceController::indexAttendance');
$routes->get('fetch-attendance/fetchRegisters', 'SessionAttendanceController::fetchRegisters');

