<?php 
namespace App\Controllers;


use App\Models\AdvisorUserInfo;

class AdvisorUsersInfo extends BaseController{

    public function index($status='3'){
        $advisor_user = new AdvisorUserInfo();
        $data['users'] = $advisor_user->getUsers($status);
        $data['common']= view ('Common/common');
        $data['header']= view('Common/header');
        $data['menu']= view('Common/menu');
        $data['footer']= view('Common/footer');
        $data['estado'] = $status;
        return view('AdvisorUserInfo/list', $data);
    }

    public function details($id=null){
        $advisor_user = new AdvisorUserInfo();
        $data['user'] = $advisor_user->getByID($id);
        $data['common']= view ('Common/common');
        $data['header']= view('Common/header');
        $data['menu']= view('Common/menu');
        $data['footer']= view('Common/footer');
        return view('AdvisorUserInfo/details', $data);
    }

    public function deactivate($id=null){
        $advisor_user = new AdvisorUserInfo();
        $advisor_user->deactivateById($id);
        return $this->response->redirect(base_url('listView/'.'3'));
    }

    public function activate($id=null){
        $advisor_user = new AdvisorUserInfo();
        $advisor_user->activateById($id);
        return $this->response->redirect(base_url('listView/'.'3'));
    }

    public function process($id=null){
        $advisor_user_info = new AdvisorUserInfo();
        $data['common']= view ('Common/common');
        $data['header']= view('Common/header');
        $data['menu']= view('Common/menu');
        $data['footer']= view('Common/footer');
        $data['errors']= [];
        if ($this->request->getPost()!=null){
            if ($this->validate($advisor_user_info->validationRules)){
                $advisor_user_info->addUpdate($id, $this->request->getPost());
                return $this->response->redirect(base_url('listView/'.'3'));
            } else {
                $data['errors'] = $this->validator->getErrors();
                $data['user']=$advisor_user_info->where('id', $id)->first();
                return view('AdvisorUserInfo/edit', $data);
            }
        } else {
            if (null!=$id){ 
                $data['user']=$advisor_user_info->where('id', $id)->first();
            } 
            
            return view('AdvisorUserInfo/edit', $data);
        }
    }

}  