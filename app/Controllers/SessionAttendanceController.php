<?php 
namespace App\Controllers;

use CodeIgniter\Controller;
use App\Models\SessionAttendance;
class SessionAttendanceController extends BaseController{

    public function indexAttendance(){
        $data['common']= view ('Common/common');
        $data['header']= view('Common/header');
        $data['menu']= view('Common/menu');
        $data['footer']= view('Common/footer');
        return view ('Attendance/attendanceIndex', $data);
    }

    public function fetchRegisters(){
        $attendance = new SessionAttendance();
        $registers['sesion'] = $attendance->getTotalAttendance('1');
        return $this->response->setJSON($registers);
    }
}