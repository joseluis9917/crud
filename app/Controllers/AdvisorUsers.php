<?php 
namespace App\Controllers;

use CodeIgniter\Controller;
use App\Models\AdvisorUser;

class AdvisorUsers extends BaseController{
    /** 
     * @var HTTP\IncomingRequest
    */
    protected $request;
    protected $helpers = ['form'];
    public $validationRules = [
        'id' => [
            'rules' => 'required|exact_length[9]',
            'errors' => [
                'required' => 'id requerido',
            ],
        ],
        'usuario' => [
            'rules' => 'required|max_length[250]',
            'errors' => [
                'required' => 'usuario requerido',
            ],
        ],
    ];

    public function index(){
        $session = \Config\Services::session();
        $data['header']= view('AdvisorUserTemplates/header');
        $data['footer']= view('AdvisorUserTemplates/footer');
        $session->remove('idUser');
        return view('AdvisorUser/create', $data);
    }

    public function save_advisor_user(){
        $session = \Config\Services::session();
        $advisor_user = new AdvisorUser();
        if ($this->validate($this->validationRules)){
            $advisor_user->add($this->request->getPost());
        } else {
            $data['header']= view('AdvisorUserTemplates/header');
            $data['footer']= view('AdvisorUserTemplates/footer');
            return view('AdvisorUser/create', $data);
        }
        $session->set('idUser', $this->request->getVar('id'));
        return $this->response->redirect(site_url('/list'));
    }
}