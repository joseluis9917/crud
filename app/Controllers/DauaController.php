<?php 
namespace App\Controllers;

use CodeIgniter\Controller;
use App\Models\Daua;
use PhpOffice\PhpSpreadsheet\IOFactory;
use CodeIgniter\I18n\Time;

class DauaController extends BaseController{
    public $validationRulesDaua = [
        'nombre' => [
            'rules' => 'required|max_length[250]',
            'errors' => [
                'required' => 'nombre requerido',
            ],
        ],
    ];
    
    public function indexDaua($status='3'){
        $daua = new Daua();
        $data['dauaAll'] = $daua->getDaua($status);
        $data['common']= view ('Common/common');
        $data['header']= view('Common/header');
        $data['menu']= view('Common/menu');
        $data['footer']= view('Common/footer');
        $data['estado'] = $status;
        return view ('Daua/daua', $data);
    }

    public function detailsDaua($id=null){
        $daua = new Daua();
        $data['daua'] = $daua->getByIdDaua($id);
        $data['common']= view ('Common/common');
        $data['header']= view('Common/header');
        $data['menu']= view('Common/menu');
        $data['footer']= view('Common/footer');
        return view('Daua/detailsDaua', $data);
    }

    public function deactivateDaua($id=null){
        $daua = new Daua();
        $daua->activeStateIdDaua($id, 0);
        return $this->response->redirect(base_url('daua/'.'3'));
    }

    public function activateDaua($id=null){
        $daua = new Daua();
        $daua->activeStateIdDaua($id, 1);
        return $this->response->redirect(base_url('daua/'.'3'));
    }

    public function processDaua($id=null){  
        $daua = new Daua();
        $data['common']= view ('Common/common');
        $data['header']= view('Common/header');
        $data['menu']= view('Common/menu');
        $data['footer']= view('Common/footer');
        $data['errors']= [];
        if ($this->request->getPost()!=null){
            if ($this->validate($this->validationRulesDaua)){
                $daua->addUpdateDaua($id, $this->request->getPost());
                return $this->response->redirect(base_url('daua/'.'3'));
            } else {
                $data['errors'] = $this->validator->getErrors();
                $data['daua']= $daua->where('id_daua', $id)->first();
                return view('Daua/editDaua', $data);
            }
        } else {
            if (null!=$id){
                $data['daua']= $daua->where('id_daua', $id)->first();
            } 
            return view('Daua/editDaua', $data);
        }
    }

}