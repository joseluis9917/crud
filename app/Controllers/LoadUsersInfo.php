<?php 
namespace App\Controllers;

use App\Models\LoadAdvisorUserInfo;
use PhpOffice\PhpSpreadsheet\IOFactory;
use App\Models\Daua;
use CodeIgniter\I18n\Time;

class LoadUsersInfo extends BaseController{

    public function indexLoadUsers(){
        $data['common']= view ('Common/common');
        $data['header']= view('Common/header');
        $data['menu']= view('Common/menu');
        $data['footer']= view('Common/footer');
        return view('LoadAdvisorUserInfo/list_loaded_users', $data);
    }

    public function fetchLoadedUsers(){
        $advisor_user = new LoadAdvisorUserInfo();
        $data['users'] = $advisor_user->getUsers('3');
        return $this->response->setJSON($data);
    }

    public function upload(){
        $data['common']= view ('Common/common');
        $data['header']= view('Common/header');
        $data['menu']= view('Common/menu');
        $data['footer']= view('Common/footer');
        $data['errors'] = [];
        return view ('LoadAdvisorUserInfo/upload', $data);
    }

    public function fileUpload(){
        $data['common']= view ('Common/common');
        $data['header']= view('Common/header');
        $data['menu']= view('Common/menu');
        $data['footer']= view('Common/footer');

        $excelValidationRule = [
            'userfile' => [
                'rules' => [
                    'uploaded[userfile]',
                    'ext_in[userfile,csv,xlsx,xls]',
                    'max_size[userfile,2048]',
                ],
                'errors' => [
                    'ext_in' => 'Formato de archivo no valido.',
                    'max_size' => 'El tamaño del archivo es demasiado grande.',
                ],
            ],
        ];

        if(!$this->validate($excelValidationRule)){
            $data['errors'] = $this->validator->getErrors();
            return view ('LoadAdvisorUserInfo/upload', $data);
        } 
        $excelFile = $this->request->getFile('userfile');
        if(!$excelFile->hasMoved()){
            $myTime = new Time('now','America/Mexico_City', 'en_US');
            $res= str_replace(':','-',$myTime->toDateTimeString());
            $newName = $res.'-'.$excelFile->getName();
            $excelFile->move('../public/temp', $newName);
            $nam = '../public/temp/'. $newName;
            $data['filename']=$nam;
            $data['table'] = $this->readExcelFile($nam);
            return view ('LoadAdvisorUserInfo/upload_success', $data);
        } else {
            $data = ['errors' => 'El archivo ya ha sido movido'];
            return view ('LoadAdvisorUserInfo/upload', $data);
        }
    }

    public function uploadToDatabase(){
        $data['associated_advisors']= [];
        $data['advisor_duplicity']= [];
        $data['missing_director']= [];
        $data['missing_teachers']= [];
        $data['missing_students']= [];
        $data['missing_advisors']= [];

        $table = $this->readExcelFile($this->request->getPost()['filename']);
        $load_advisor_user = new LoadAdvisorUserInfo();
        $daua = new Daua();
        $load_advisor_user->emptyCargaConsejero();
        $flashdata = session();
        foreach($table as $user){
            $convertCumpleanos = date("Y-m-d", strtotime($user['cumpleanos']));
            $convertProtesta = date("Y-m-d", strtotime($user['protesta']));
            $dauaArr = $daua->getByIdDaua($user['daua']);
            $user['daua'] = $dauaArr['id_daua'];
            if (strcasecmp($user['activo'], 'activo')==0){
                $user['activo'] = '1';
            } else if (strcasecmp($user['activo'], 'inactivo')==0){
                $user['activo'] = '0';
            }

            if (strcasecmp($user['tipo'], 'propietario')==0){
                $user['tipo'] = '1';
            } else if (strcasecmp($user['tipo'], 'suplente')==0){
                $user['tipo'] = '2';
            }

            if(strcasecmp($user['cargo'], 'director')==0){
                $user['cargo'] = '1';
            } else if (strcasecmp($user['cargo'], 'docente')==0){
                $user['cargo'] = '2';
            }else if (strcasecmp($user['cargo'], 'alumno')==0){
                $user['cargo'] = '3';
            } else if (strcasecmp($user['cargo'], 'adjunto')==0){
                $user['cargo'] = '4';
            }
            $info = array(
                'curul' => $user['curul'],
                'id' => $user['id'],
                'consejero_asociado' => $user['consejero_asociado'],
                'daua' => $user['daua'],
                'grado' => $user['grado'],
                'nombre' => $user['nombre'],
                'apellido_paterno' => $user['apellido_paterno'],
                'apellido_materno' => $user['apellido_materno'],
                'cumpleanos' => $convertCumpleanos,
                'cargo' => $user['cargo'],
                'tipo' => $user['tipo'],
                'tel_cel_1' => $user['tel_cel_1'],
                'tel_cel_1_whats' => $user['tel_cel_1_whats'],
                'tel_cel_2' => $user['tel_cel_2'],
                'tel_cel_2_whats' => $user['tel_cel_2_whats'],
                'tel_casa' => $user['tel_casa'],
                'tel_oficina' => $user['tel_oficina'],
                'extension' => $user['extension'],
                'correo_institucional' => $user['correo_institucional'],
                'correo_personal' => $user['correo_personal'],
                'comision' => $user['comision'],
                'protesta' => $convertProtesta,
                'activo' => $user['activo'],
                'foto' => $user['foto']
            );
            
            if ($user['id']!=null){
                $load_advisor_user->addUpdate(null, $info);
            }
        }
        foreach($table as $user){
            $dauaArr = $daua->getByIdDaua($user['daua']);
            if ($load_advisor_user->searchUser($user['consejero_asociado'])==null && $user['consejero_asociado']!=null){
                array_push($data['associated_advisors'], $user['consejero_asociado']);
            } else if (count($load_advisor_user->checkDuplicity($user['id']))>=2){
                array_push($data['advisor_duplicity'], array('id' => $user['id'], 'nombre' => $user['nombre'].' '.$user['apellido_paterno'].' '.$user['apellido_materno']));
            }
        }

        $numCurul = $load_advisor_user->countCurulByDaua();

        foreach ($numCurul as $unidad){
            $dauaArr = $daua->getByIdDaua($unidad['nombre']);

            if ($dauaArr['id_daua']!='44'){
                if ($unidad['total']<'9'){
                    $aux = 9-(int)$unidad['total'];
                    array_push($data['missing_advisors'], array('id_daua'=> $dauaArr['id_daua'],'nombre' => $dauaArr['nombre'], 'num_adv' => $aux));
                }
                if ($unidad['tot_dir']<'1'){
                    array_push($data['missing_director'], array('id_daua'=> $dauaArr['id_daua'],'nombre' => $dauaArr['nombre']));
                }
                if ($unidad['tot_docente']<'4'){
                    $aux = 4-(int)$unidad['tot_docente'];
                    array_push($data['missing_teachers'], array('id_daua'=> $dauaArr['id_daua'],'nombre' => $dauaArr['nombre'], 'num_adv' => $aux));
                }
                if ($unidad['tot_alumno']<'4'){
                    $aux = 4-(int)$unidad['tot_alumno'];
                    array_push($data['missing_students'], array('id_daua'=> $dauaArr['id_daua'],'nombre' => $dauaArr['nombre'], 'num_adv' => $aux));
                }
            }

            if ($dauaArr['id_daua']=='44'){
                $data['missing_adjoints']= [];

                if ($unidad['total']<'6'){
                    $aux = 6-(int)$unidad['total'];
                    array_push($data['missing_advisors'],  array('id_daua'=> $dauaArr['id_daua'],'nombre' => $dauaArr['nombre'], 'num_adv' => $aux));
                }
                if ($unidad['tot_adjunto']<'4'){
                    $aux = 4-(int)$unidad['tot_alumno'];
                    array_push($data['missing_adjoints'],  array('id_daua'=> $dauaArr['id_daua'],'nombre' => $dauaArr['nombre'], 'num_adv' => $aux));
                }
            }
        }
        $flashdata->setFlashdata('errors', $data);
        //return view('LoadAdvisorUserInfo/list_loaded_users_success', $data);
        return $this->response->redirect(base_url('fetchLoadUsersView'));
    }

    public function verifyStructure(){
        $load_advisor_user = new LoadAdvisorUserInfo();
        $users = $load_advisor_user->getUsers('3');
        $data['associated_advisors']= [];
        $data['advisor_duplicity']= [];
        $data['missing_director']= [];
        $data['missing_teachers']= [];
        $data['missing_students']= [];
        $data['missing_advisors']= [];
        $daua = new Daua();
        
        foreach($users as $user){
            switch ($user['cargo']){
                case '1':
                    $user['cargo'] = 'Director';
                    break;
                case '2':
                    $user['cargo'] = 'Docente';
                    break;
                case '3':
                    $user['cargo'] = 'Alumno';
                    break;
                case '4':
                    $user['cargo'] = 'Adjunto';
                    break;
            }
            switch ($user['tipo']){
                case '1':
                    $user['tipo'] ='Propietario';
                    break;
                case '2':
                    $user['tipo'] ='Suplente';
                    break;
            }
            if (($load_advisor_user->searchUser($user['consejero_asociado'])==null) && $user['consejero_asociado']!='0' && $user['consejero_asociado']!=null){
                array_push($data['associated_advisors'], $user['consejero_asociado']);
            }else if (count($load_advisor_user->checkDuplicity($user['id']))>=2){
                array_push($data['advisor_duplicity'], array('id' => $user['id'], 'nombre' => $user['nombre'].' '.$user['apellido_paterno'].' '.$user['apellido_materno']));
            }
        }

        $numCurul = $load_advisor_user->countCurulByDaua();
        foreach ($numCurul as $unidad){
            $errorString = '';
            $dauaArr = $daua->getByIdDaua($unidad['nombre']);
            if ($dauaArr['id_daua']!='44'){
                if ($unidad['total']<'9'){
                    $aux = 9-(int)$unidad['total'];
                    array_push($data['missing_advisors'], array('id_daua'=> $dauaArr['id_daua'],'nombre' => $dauaArr['nombre'], 'num_adv' => $aux));
                    if ($unidad['tot_dir']<'1'){
                        array_push($data['missing_director'], array('id_daua'=> $dauaArr['id_daua'],'nombre' => $dauaArr['nombre']));
                    }
                    if ($unidad['tot_docente']<'4'){
                        $aux = 4-(int)$unidad['tot_docente'];
                        array_push($data['missing_teachers'], array('id_daua'=> $dauaArr['id_daua'],'nombre' => $dauaArr['nombre'], 'num_adv' => $aux));
                    }
                    if ($unidad['tot_alumno']<'4'){
                        $aux = 4-(int)$unidad['tot_alumno'];
                        array_push($data['missing_students'], array('id_daua'=> $dauaArr['id_daua'],'nombre' => $dauaArr['nombre'], 'num_adv' => $aux));
                    }
                }
                
            } 

            if ($dauaArr['id_daua']=='44'){
                $data['missing_adjoints']= [];

                if ($unidad['total']<'6'){
                    $aux = 6-(int)$unidad['total'];
                    array_push($data['missing_advisors'],  array('id_daua'=> $dauaArr['id_daua'],'nombre' => $dauaArr['nombre'], 'num_adv' => $aux));
                    if ($unidad['tot_adjunto']<'4'){
                        $aux = 4-(int)$unidad['tot_alumno'];
                        array_push($data['missing_adjoints'],  array('id_daua'=> $dauaArr['id_daua'],'nombre' => $dauaArr['nombre'], 'num_adv' => $aux));
                    }
                }
                }
        }
        return $this->response->setJSON($data);
    }

    public function readExcelFile($nam){
        $spreadsheet = IOFactory::load($nam);
        $worksheet = $spreadsheet->getActiveSheet();
        $index = array(
            'curul',
            'id',
            'consejero_asociado',
            'daua',
            'grado',
            'nombre',
            'apellido_paterno',
            'apellido_materno',
            'cumpleanos',
            'cargo',
            'tipo' ,
            'tel_cel_1',
            'tel_cel_1_whats',
            'tel_cel_2',
            'tel_cel_2_whats',
            'tel_casa',
            'tel_oficina',
            'extension',
            'correo_institucional',
            'correo_personal',
            'comision',
            'protesta',
            'activo',
            'foto'
        );
        foreach($worksheet->getRowIterator() AS $row){
            $cellIterator = $row->getCellIterator();
            $cellIterator->setIterateOnlyExistingCells(FALSE);
            $cells=[];
            $aux = 0;
            foreach($cellIterator as $cell){
                $cells[$index[$aux]] = $cell->getFormattedValue();
                $aux ++;
            }
            $rows[] = $cells;
        }
        unset($rows[0]);
        return $rows;
    }


    public function processLoadedUser($id=null){
        $load_advisor_user = new LoadAdvisorUserInfo();
        $data['common']= view ('Common/common');
        $data['header']= view('Common/header');
        $data['menu']= view('Common/menu');
        $data['footer']= view('Common/footer');
        $data['id'] = $id;
        if ($this->request->getPost()!=null){
            $load_advisor_user->addUpdate($id, $this->request->getPost());
            return $this->response->redirect(base_url('fetchLoadUsersView'));
        } else {
            if (null!=$id){ 
                $data['user']=$load_advisor_user->where('id', $id)->first();
            } 
        }
        return view('LoadAdvisorUserInfo/edit_loaded_users', $data);
    }
    
}