<?php 
namespace App\Models;
use CodeIgniter\Model;

class Daua extends Model{
    protected $table      = 'daua';
    protected $primaryKey = 'id_daua';
    protected $allowedFields = ['id_daua', 'nombre', 'activo', 'clave', 'clave_coll'];

    public function addUpdateDaua($id, $data){
        if(null!=$id){
            $this->update($id, $data);
        } else {
            $this->insert($data);
        }
    }

    public function getDaua($state){
        $this->select('*');
        if ($state!=0 && $state!=1){
            $query = $this->get();
            return $query->getResultArray();
        } else {
            $this->where('activo', $state);
            $query = $this->get();
            return $query->getResultArray();
        }
    }

    public function getByIdDaua($id){
        $this->select('*');
        if (is_numeric($id)){
            return $this->where('id_daua', $id)->first();
        } else {
            return $this->where('nombre', $id)->first();
        }
    }

    public function activeStateIdDaua($id=null, $state){
        $this->set('activo', $state)
                  ->where('id_daua', $id)
                  ->update();
    }
}
