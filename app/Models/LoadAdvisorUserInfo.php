<?php 
namespace App\Models;

use CodeIgniter\Model;


class LoadAdvisorUserInfo extends Model{
    protected $table      = 'carga_consejero';
    // Uncomment below if you want add primary key
    protected $primaryKey = 'num_usuario';
    protected $allowedFields = ['num_usuario' ,'curul', 'id','consejero_asociado', 'daua', 'grado', 'nombre','apellido_paterno','apellido_materno','cumpleanos','cargo','tipo', 'tel_cel_1','tel_cel_1_whats','tel_cel_2','tel_cel_2_whats','tel_casa','tel_oficina','extension','correo_institucional','correo_personal','comision','protesta','activo', 'foto'];

    public function addUpdate($id, $data){
        if($id!=null){
            $num_usuario = $this->getNumUsuario($id, $data);
            $this->update($num_usuario, $data);
        } else {
            $this->insert($data);
        }
    }

    public function emptyCargaConsejero(){
        $this->emptyTable('carga_consejero');
    }

    public function getNumUsuario($id, $data){
        $aux = $this->where('id', $id)
        ->where('nombre', $data['nombre'])
        ->where('apellido_paterno', $data['apellido_paterno'])->first();
        return $aux['num_usuario'];
    }

    public function searchUser($id){
        return $this->where('id', $id)->first();
    }

    public function checkDuplicity($id){
        $this->select('id')
            ->select('cargo')
            ->select('tipo');
        $this->where('id', $id );
        $query = $this->get();
        return $query->getResultArray();
    }

    public function countCurulByDaua(){
        $sql =  '
        SELECT
            daua.id_daua,
            daua.nombre,
            sum(if(carga_consejero.cargo=1,1,0)) as tot_dir,
            sum(if(carga_consejero.cargo=2,1,0)) as tot_docente,
            sum(if(carga_consejero.cargo=3,1,0)) as tot_alumno,
            sum(if(carga_consejero.cargo=4,1,0)) as tot_adjunto,
            count(carga_consejero.num_usuario) as total
        FROM
            daua
        left Join carga_consejero ON daua.id_daua = carga_consejero.daua
        group by daua.id_daua';
        return  $this->query($sql)->getResultArray();
    }

    public function searchDaua($daua){
        return $this->where('daua', $daua)->first();
    }
    
    public function getUsers($state){
        $this->select('*');
        if ($state!=0 && $state!=1){
            $query = $this->get();
            return $query->getResultArray();
        } else {
            $this->where('activo', $state);
            $query = $this->get();
            return $query->getResultArray();
        }
    }
}