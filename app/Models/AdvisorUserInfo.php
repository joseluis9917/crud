<?php 
namespace App\Models;
use CodeIgniter\Model;
use CodeIgniter\I18n\Time;

class AdvisorUserInfo extends Model{
    protected $table      = 'informacion_consejero';
    // Uncomment below if you want add primary key
    protected $primaryKey = 'id';
    protected $allowedFields = ['id', 'grado', 'nombre','apellido_paterno','apellido_materno','cumpleanos','cargo','tipo', 'daua', 'tel_cel_1','tel_cel_1_whats','tel_cel_2','tel_cel_2_whats','tel_casa','tel_oficina','extension','correo_institucional','correo_personal','comision','protesta','consejero_asociado','activo', 'foto'];

    public $validationRules = [
        'id' => [ 'rules' => 'required|exact_length[9]', 'errors' => [ 'required' => 'id requerido',], ],
        'grado' => [ 'rules' => 'required|max_length[20]', 'errors' => [ 'required' => 'grado requerido', ], ],
        'nombre' => [ 'rules' => 'required|max_length[100]', 'errors' => [ 'required' => 'nombre requerido', ], ],
        'apellido_paterno' => [ 'rules' => 'required|max_length[100]', 'errors' => [ 'required' => 'apellido paterno requerido', ], ],
        'apellido_materno' => [ 'rules' => 'required|max_length[100]', 'errors' => [ 'required' => 'apellido materno requerido', ],],
        'cumpleanos' => [ 'rules' => 'required', 'errors' => [ 'required' => 'cumple requerido', ], ],
        'cargo' => [ 'rules' => 'required|integer', 'errors' => [ 'required' => 'cargo requerido', ], ],
        'tipo' => [ 'rules' => 'required|in_list[1,2]|integer', 'errors' => [ 'required' => 'tipo requerido', ], ],
        'daua' => [ 'rules' => 'required|max_length[100]', 'errors' => [ 'required' => 'daua requerido', ], ],
        'tel_cel_1' => [ 'rules' => 'required|max_length[100]', 'errors' => [ 'required' => 'tel_cel_1 requerido', ], ],
        'tel_cel_1_whats' => [ 'rules' => 'max_length[1]|integer', ],
        'tel_cel_2' => [ 'rules' => 'max_length[20]', ],
        'tel_cel_2_whats' => [ 'rules' => 'max_length[1]|integer', ],
        'tel_casa' => [ 'rules' => 'required|max_length[20]', 'errors' => [ 'required' => 'tel_casa requerido', ], ],
        'tel_oficina' => [ 'rules' => 'max_length[20]', ],
        'extension' => [ 'rules' => 'max_length[20]', ],
        'correo_institucional' => [ 'rules' => 'required|max_length[250]', 'errors' => [ 'required' => 'correo_institucional requerido', ], ],
        'correo_personal' => [ 'rules' => 'required|max_length[250]', 'errors' => [ 'required' => 'correo_personal requerido', ], ],
        'comision' => [ 'rules' => 'is_natural_no_zero|max_length[10]', ],
        'protesta' => [ 'rules' => 'required', 'errors' => [ 'required' => 'protesta requerido', ], ],
        'consejero_asociado' => [ 'rules' => 'required|integer|max_length[10]', 'errors' => [ 'required' => 'consejero_asociado requerido', ], ],
        'foto' => [ 'rules' => 'required|max_length[250]', 'errors' => [ 'required' => 'foto requerido', ], ],
    ];

    public function addUpdate($id, $data){
        $advisor_user = new AdvisorUser();
        if(null!=$id){
            $this->update($id, $data);
        } else {
            $user= array(
                'id' => $data['id'],
                'usuario' => $data['id'], 
                'passwd' => '$2y$10$POTsJX5wH3vhf4MNOtwI/ORnFJ4FG8nnbkjXC14DeCfsrCujFntj2'
            );
            $advisor_user->add($user);
            $this->insert($data);
        }
    }

    public function search($id){
        return $this->where('id', $id)->first();
    }
    
    public function getUsers($state){
        $this->select('*');
        $this->join('usuarios_consejeros', 'informacion_consejero.id = usuarios_consejeros.id', 'left');
        if ($state!=0 && $state!=1){
            $query = $this->get();
            return $query->getResultArray();
        } else {
            $this->where(['informacion_consejero.activo' => $state]);
            $query = $this->get();
            return $query->getResultArray();
        }
    }

    public function getByID($id){
        $this->select('*');
        $this->join('usuarios_consejeros','informacion_consejero.id = usuarios_consejeros.id', 'left');
        return $this->where('usuarios_consejeros.id', $id)->first();
    }

    public function deactivateById($id=null){
        $myTime = new Time('now','America/Mexico_City', 'en_US');
        $db      = \Config\Database::connect();
        $builder = $db->table('usuarios_consejeros');
        $builder->set('fecha_inactividad', $myTime);
        $builder->set('activo', '0')
                  ->where('id', $id)
                  ->update();
        $db      = \Config\Database::connect();
        $builder = $db->table('informacion_consejero');
        $builder->set('activo', '0')
                  ->where('id', $id)
                  ->update();
    }

    public function activateById($id=null){
        $db      = \Config\Database::connect();
        $builder = $db->table('usuarios_consejeros');
        $builder->set('activo', '1')
                  ->where('id', $id)
                  ->update();
        $this->set('activo', '1')
                  ->where('id', $id)
                  ->update();
    }

}
