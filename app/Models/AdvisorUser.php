<?php 
namespace App\Models;

use CodeIgniter\Model;

class AdvisorUser extends Model{
    protected $table      = 'usuarios_consejeros';
    // Uncomment below if you want add primary key
    protected $primaryKey = 'id_usuarios_consejeros';
    protected $allowedFields = ['id_usuarios_consejeros', 'id', 'usuario', 'passwd', 'activo', 'passwd_cambio', 'rol_idrol', 'info_cambio'];

    public function add($data){
        $this->insert($data);
    }

    public function search($id){
        return $this->where('id', $id)->first();
    }
}