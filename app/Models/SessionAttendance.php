<?php 
namespace App\Models;

use CodeIgniter\Model;

class SessionAttendance extends Model{
    protected $table      = 'asistencia';
    // Uncomment below if you want add primary key
    protected $primaryKey = 'id_asistencia';
    protected $allowedFields = ['id_asistencia', 'id_usuario', 'id_sesion', 'asistencia', 'activo', 'voto', 'propietario', 'id_usuario_suplente', 'justificante', 'descripcion_justificante'];

    public function getTotalAttendance($id){
        $this->where('id_sesion',$id);
        return $this->countAllResults();
    }
}